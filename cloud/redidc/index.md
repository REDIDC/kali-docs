---
title: Redidc
description:
icon:
weight:
author: ["linrong",]
---


[REDIDC](https://redidc.net) provide cloud servers based on Kali's official image. There are multiple versions to choose from. The basic hardware configuration is provided by default and can be upgraded as required.

Create an instance：

![Create](redidc-1.png)

The root user has been enabled and xrdp has been installed. The user can freely choose to use the command line or the graphical interface link. SSH has been enabled. The graphical interface link uses port 3389. Everything is default.

![Connecting](redidc-2.png)
